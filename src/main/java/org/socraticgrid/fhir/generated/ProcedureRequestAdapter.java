package org.socraticgrid.fhir.generated;

import org.socraticgrid.fhir.generated.IProcedureRequest;
import ca.uhn.fhir.model.dstu2.resource.ProcedureRequest;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.composite.CodingDt;
import java.util.ArrayList;
import java.util.Iterator;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import java.util.Date;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.dstu2.resource.Practitioner;
import ca.uhn.fhir.model.dstu2.resource.Organization;
import ca.uhn.fhir.model.dstu2.resource.RelatedPerson;
import ca.uhn.fhir.model.dstu2.valueset.ProcedureRequestStatusEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.primitive.StringDt;
import java.lang.String;
import ca.uhn.fhir.model.primitive.BooleanDt;
import ca.uhn.fhir.model.dstu2.resource.Device;
import ca.uhn.fhir.model.dstu2.valueset.ProcedureRequestPriorityEnum;

public class ProcedureRequestAdapter implements IProcedureRequest
{

   private ProcedureRequest adaptedClass = new ProcedureRequest();

   public IdDt getId()
   {
      return adaptedClass.getId();
   }

   public void setId(IdDt param)
   {
      adaptedClass.setId(param);
   }

   public CodeDt getLanguage()
   {
      return adaptedClass.getLanguage();
   }

   public void setLanguage(CodeDt param)
   {
      adaptedClass.setLanguage(param);
   }

   public NarrativeDt getText()
   {
      return adaptedClass.getText();
   }

   public void setText(NarrativeDt param)
   {
      adaptedClass.setText(param);
   }

   public ContainedDt getContained()
   {
      return adaptedClass.getContained();
   }

   public void setContained(ContainedDt param)
   {
      adaptedClass.setContained(param);
   }

   public List<IdentifierDt> getIdentifier()
   {
      return adaptedClass.getIdentifier();
   }

   public void setIdentifier(List<IdentifierDt> param)
   {
      adaptedClass.setIdentifier(param);
   }

   public void addIdentifier(IdentifierDt param)
   {
      adaptedClass.getIdentifier().add(param);
   }

   public Patient getSubjectResource()
   {
      if (adaptedClass.getSubject().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Patient)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Patient) adaptedClass
               .getSubject().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setSubjectResource(Patient param)
   {
      adaptedClass.getSubject().setResource(param);
   }

   public CodeableConceptDt getType()
   {
      return adaptedClass.getType();
   }

   public void setType(CodeableConceptDt param)
   {
      adaptedClass.setType(param);
   }

   public List<String> getTypeAsStringList() {
		List<String> codes = new ArrayList<>();
		List<CodingDt> codings = adaptedClass.getType().getCoding();
		for (Iterator<CodingDt> iterator = codings.iterator(); iterator
				.hasNext();) {
			CodingDt codingDt = (CodingDt) iterator.next();
			codes.add(codingDt.getCode());
		}
		return codes;
	}

   public List<ProcedureRequest.BodySite> getBodySite()
   {
      return adaptedClass.getBodySite();
   }

   public void setBodySite(List<ProcedureRequest.BodySite> param)
   {
      adaptedClass.setBodySite(param);
   }

   public void addBodySite(ProcedureRequest.BodySite param)
   {
      adaptedClass.getBodySite().add(param);
   }

   public List<CodeableConceptDt> getIndication()
   {
      return adaptedClass.getIndication();
   }

   public void setIndication(List<CodeableConceptDt> param)
   {
      adaptedClass.setIndication(param);
   }

   public void addIndication(CodeableConceptDt param)
   {
      adaptedClass.getIndication().add(param);
   }

   public DateTimeDt getTimingDateTimeElement()
   {
      if (adaptedClass.getTiming() != null
            && adaptedClass.getTiming() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return (ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getTiming();
      }
      else
      {
         return null;
      }
   }

   public Date getTimingDateTime()
   {
      if (adaptedClass.getTiming() != null
            && adaptedClass.getTiming() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return ((ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getTiming()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setTimingDateTime(DateTimeDt param)
   {
      adaptedClass.setTiming(param);
   }

   public void setTimingDateTime(Date param)
   {
      adaptedClass
            .setTiming(new ca.uhn.fhir.model.primitive.DateTimeDt(param));
   }

   public TimingDt getTimingPeriod()
   {
      if (adaptedClass.getTiming() != null
            && adaptedClass.getTiming() instanceof ca.uhn.fhir.model.dstu2.composite.TimingDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.TimingDt) adaptedClass
               .getTiming();
      }
      else
      {
         return null;
      }
   }

   public void setTimingPeriod(TimingDt param)
   {
      adaptedClass.setTiming(param);
   }

   public TimingDt getTimingTiming()
   {
      if (adaptedClass.getTiming() != null
            && adaptedClass.getTiming() instanceof ca.uhn.fhir.model.dstu2.composite.TimingDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.TimingDt) adaptedClass
               .getTiming();
      }
      else
      {
         return null;
      }
   }

   public void setTimingTiming(TimingDt param)
   {
      adaptedClass.setTiming(param);
   }

   public Encounter getEncounterResource()
   {
      if (adaptedClass.getEncounter().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Encounter)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Encounter) adaptedClass
               .getEncounter().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setEncounterResource(Encounter param)
   {
      adaptedClass.getEncounter().setResource(param);
   }

   public Practitioner getPerformerPractitionerResource()
   {
      if (adaptedClass.getPerformer().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Practitioner)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Practitioner) adaptedClass
               .getPerformer().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setPerformerResource(Practitioner param)
   {
      adaptedClass.getPerformer().setResource(param);
   }

   public Organization getPerformerOrganizationResource()
   {
      if (adaptedClass.getPerformer().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Organization)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Organization) adaptedClass
               .getPerformer().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setPerformerResource(Organization param)
   {
      adaptedClass.getPerformer().setResource(param);
   }

   public Patient getPerformerPatientResource()
   {
      if (adaptedClass.getPerformer().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Patient)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Patient) adaptedClass
               .getPerformer().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setPerformerResource(Patient param)
   {
      adaptedClass.getPerformer().setResource(param);
   }

   public RelatedPerson getPerformerRelatedPersonResource()
   {
      if (adaptedClass.getPerformer().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.RelatedPerson)
      {
         return (ca.uhn.fhir.model.dstu2.resource.RelatedPerson) adaptedClass
               .getPerformer().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setPerformerResource(RelatedPerson param)
   {
      adaptedClass.getPerformer().setResource(param);
   }

   public BoundCodeDt<ProcedureRequestStatusEnum> getStatusElement()
   {
      return adaptedClass.getStatusElement();
   }

   public String getStatus()
   {
      return adaptedClass.getStatus();
   }

   public void setStatus(String param)
   {
      adaptedClass
            .setStatus(ca.uhn.fhir.model.dstu2.valueset.ProcedureRequestStatusEnum
                  .valueOf(param));
   }

   public void setStatus(BoundCodeDt<ProcedureRequestStatusEnum> param)
   {
      adaptedClass.setStatus(param);
   }

   public BooleanDt getAsNeededBooleanElement()
   {
      if (adaptedClass.getAsNeeded() != null
            && adaptedClass.getAsNeeded() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return (ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getAsNeeded();
      }
      else
      {
         return null;
      }
   }

   public Boolean getAsNeededBoolean()
   {
      if (adaptedClass.getAsNeeded() != null
            && adaptedClass.getAsNeeded() instanceof ca.uhn.fhir.model.primitive.BooleanDt)
      {
         return ((ca.uhn.fhir.model.primitive.BooleanDt) adaptedClass
               .getAsNeeded()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setAsNeededBoolean(BooleanDt param)
   {
      adaptedClass.setAsNeeded(param);
   }

   public void setAsNeededBoolean(Boolean param)
   {
      adaptedClass.setAsNeeded(new ca.uhn.fhir.model.primitive.BooleanDt(
            param));
   }

   public CodeableConceptDt getAsNeededCodeableConcept()
   {
      if (adaptedClass.getAsNeeded() != null
            && adaptedClass.getAsNeeded() instanceof ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) adaptedClass
               .getAsNeeded();
      }
      else
      {
         return null;
      }
   }

   public void setAsNeededCodeableConcept(CodeableConceptDt param)
   {
      adaptedClass.setAsNeeded(param);
   }

   public DateTimeDt getOrderedOnElement()
   {
      return adaptedClass.getOrderedOnElement();
   }

   public Date getOrderedOn()
   {
      return adaptedClass.getOrderedOn();
   }

   public void setOrderedOn(Date param)
   {
      adaptedClass.setOrderedOn(new ca.uhn.fhir.model.primitive.DateTimeDt(
            param));
   }

   public void setOrderedOn(DateTimeDt param)
   {
      adaptedClass.setOrderedOn(param);
   }

   public Practitioner getOrdererPractitionerResource()
   {
      if (adaptedClass.getOrderer().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Practitioner)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Practitioner) adaptedClass
               .getOrderer().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setOrdererResource(Practitioner param)
   {
      adaptedClass.getOrderer().setResource(param);
   }

   public Patient getOrdererPatientResource()
   {
      if (adaptedClass.getOrderer().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Patient)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Patient) adaptedClass
               .getOrderer().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setOrdererResource(Patient param)
   {
      adaptedClass.getOrderer().setResource(param);
   }

   public RelatedPerson getOrdererRelatedPersonResource()
   {
      if (adaptedClass.getOrderer().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.RelatedPerson)
      {
         return (ca.uhn.fhir.model.dstu2.resource.RelatedPerson) adaptedClass
               .getOrderer().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setOrdererResource(RelatedPerson param)
   {
      adaptedClass.getOrderer().setResource(param);
   }

   public Device getOrdererDeviceResource()
   {
      if (adaptedClass.getOrderer().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Device)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Device) adaptedClass
               .getOrderer().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setOrdererResource(Device param)
   {
      adaptedClass.getOrderer().setResource(param);
   }

   public BoundCodeDt<ProcedureRequestPriorityEnum> getPriorityElement()
   {
      return adaptedClass.getPriorityElement();
   }

   public String getPriority()
   {
      return adaptedClass.getPriority();
   }

   public void setPriority(String param)
   {
      adaptedClass
            .setPriority(ca.uhn.fhir.model.dstu2.valueset.ProcedureRequestPriorityEnum
                  .valueOf(param));
   }

   public void setPriority(BoundCodeDt<ProcedureRequestPriorityEnum> param)
   {
      adaptedClass.setPriority(param);
   }

   public ProcedureRequest getAdaptee()
   {
      return adaptedClass;
   }

   public void setAdaptee(ProcedureRequest param)
   {
      this.adaptedClass = param;
   }
}